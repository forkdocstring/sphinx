"""
This module contains the default columns and metadata tables naming, accordingly to the database standard.
Any change to the database should be reflected here.
"""

import numpy as np
import pandas as pd


def func(eps=0.1, min_samples='auto', votes=3, min_consecutive=6, filters=None):
    """
    Finds performance deviations due to environmental conditions, unavailability, and, more importantly, derate. The
    points are classified (type) accordingly:

    0 - Available

    1 - Scattering

    2 - Environmental

    3 - Unavailable

    4 - Derate

    The classification consists of two steps: basic filtering and density based clusterization. Three scatter plots
    are employed (pwr-nws; pwr-rot-spd pitch-pwr) and their cluster labelings are crossed in order to find the type
    of each point.

    :param eps: the maximum (normalized) distance between two samples for one to be considered as in the neighborhood of
        the other. Defaults to 0.1
    :type eps: float
    :param min_samples: the number of samples (or total weight) in a neighborhood for a point to be considered as a core
        point. This includes the point itself. Defaults to 'auto'
    :type min_samples: int or 'auto'
    :param votes: number of votes for a point to be considered 'scattering'. Defaults to 3 (unanimity)
    :type votes: int
    :param min_consecutive: minimum number of consecutive scattering points to be classified as derate
    :type min_consecutive: int
    :param filters: list of filters applied sequentially before clustering. The filter syntax is Pandas query like and
        it is defined in 'conf/config.yaml'. Defaults to '[normality]'
    """


def func2(park, turbine=None, name=None, start=None, end=None,
          days=60, return_data=False, output=None, timing=False):
    """
    Execute the pipeline for one turbine at a time.

    :param park: name of the park (or site)
    :type park: str
    :param turbine: number of the turbine. Defaults to None
    :type turbine: int or None
    :param name: alternatively, instead of number, use name of the turbine. Defaults to None
    :type name: str or None
    :param start: start date. Defaults to None
    :type start: str or None
    :param end: end date. Defaults to None
    :type end: str or None
    :param days: number of days back before the most recent record, if 'start' and 'end' both left None.
        Defaults to 60
    :type days: int
    :param return_data: return raw input data. Defaults to False
    :type return_data: bool
    :param output: specifies the output location (see it for the specific sink). Defaults to None
    :param timing: measure and print the amount of time spent in component processing.Defaults to False
    :type timing: bool
    :return: result
    :rtype: pandas DataFrame
    :raises TypeError: if more than one turbine is requested (list[int], not int)

    Examples:

    >>> import numpy as np
    >>> import pandas as pd
    >>> x = np.array([0, 1, 2, 3])
    >>> df = pd.DataFrame({'A': x})
    """
    return pd.DataFrame()
